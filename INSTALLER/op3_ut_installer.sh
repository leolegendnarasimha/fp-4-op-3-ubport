#!/bin/bash

# A script for flashing an image over telnet.
# This is required for e.g. OnePlus 3/3T, because fastboot refuses to flash images over a certain size.

IMAGE="ubuntu.img"
PARTITION="system"
TELNET_ADDRESS="192.168.2.15"
#If you specify a port, add a space before it, for example: TELNET_PORT=" 23"
TELNET_PORT=""

check_deps() {
	has_deps=true
	for cmd in "$@"; do
		echo "Checking for $cmd..."
		if [ -z "$(command -v "$cmd")" ]; then
			echo "$cmd is required but not available."
			has_deps=false
		else
			echo "$cmd is available."
		fi
	done
	if $has_deps; then
		echo "All dependencies dependencies found."
	else
		echo "Install the required dependencies and then run the script again."
		exit 1
	fi
}

check_deps fastboot telnet nc pv

if [ -f "${IMAGE}" ]; then
    echo "${IMAGE} found, starting the flashing process..."
else
    echo "${IMAGE} not found in current directory. Exiting..."
    exit 1
fi

# reboot device and wait for telnet
fastboot reboot
sleep 15
telnet_available=false
for attempt in {1..5}; do
	echo "$attempt. attempt to telnet find telnet connection"

	if ping -c3 "${TELNET_ADDRESS}"; then
		echo "telnet connection available"
		telnet_available=true
		break
	else
		sleep 10
	fi
done
if ! $telnet_available; then
	echo "Couldn't connect to device via telnet"
	exit 1
fi

# 6000 seconds is 100 minutes. It takes around 6 minutes to flash system.img, so this should be more than enough
( (echo "nc -l -p 12345 > /dev/disk/by-partlabel/${PARTITION}" && sleep 6000) | telnet "${TELNET_ADDRESS}${TELNET_PORT}" ) &
sleep 5
if pv "${IMAGE}" | nc -q 0 "${TELNET_ADDRESS}" 12345; then
    echo "flashed successfully"
    sleep 5
    ( (echo "reboot -f" && sleep 2) | telnet "${TELNET_ADDRESS}${TELNET_PORT}" ) &
    sleep 5
    exit 0
else
    echo "failed to flash"
    exit 1
fi
