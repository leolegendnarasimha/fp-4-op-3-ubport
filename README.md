# <h1>Ubuntu Touch 20.04 GUIDE for the OnePlus 3(T) </h1>

## **_NOTE :_** [![Important](https://img.shields.io/badge/Important-brown?style=flat-square)]()

- _This process requires a Linux evironment on a PC or Laptop and make sure that you are on [latest firmware](https://github.com/Droidian-oneplus3/device-page/releases/tag/oneplus3-firmware) (i.e. OOS 9.0.6) on your handset and bootloader needs to be unlocked.
And also this process erases all the data on your OnePlus 3(T) device . Hence backup any important data before proceeding._

- _Also, This is a completely `alpha` development phase project, and more work is yet to be done, not all the features are working, so DO NOT TAKE THIS PROJECT AS EVEN `BETA` DEVELOPMENT PHASE._


<h2>Step 1 :  Treblizing the device</h2>

**Note :** _If you have already treblized the device, you can skip to [Step 2](#step2)_

In order to treblize OnePlus 3(T), we need to create and populate `/vendor` partition manually.

* Download treble patched TWRP recovery from here [TWRP_treble](https://github.com/Droidian-oneplus3/device-page/releases/tag/twrp-op3treble) and flash it through `fastboot`.

* Boot your device into recovery and head to `terminal` section , enter command `treblize` to create the `/vendor` partition.

* Head to `wipe` and select `format data` and enter `yes`. This formats data .

* Head to `reboot` and select `recovery`, if it says 'No OS is installed' and asks for swipe to confirm reboot, just do it . This reboots device back to twrp recovery.

* Again. Head to `wipe` section and select all the partitions except `usb otg` and `vendor` . And do _swipe to confirm wipe_.

### Now you can choose either of the methods below to populate the `/vendor` partition.

### _Method 1_ : Manual building method

* In this method, we build vendor image manually and flash it into device via `fastboot`.

* While building a vendor image, the steps are quite similar to those needed
to build the system image with Halium:
1. Follow instructions for building here: https://github.com/OP3-Halium/Documentation#install-prerequisites-for-building

2. Run `mka vendorimage`
This will generate a file `our/target/product/oneplus3/vendor.img` that should then be
flashed with `fastboot flash vendor vendor.img`.

3. After flashing the `vendor.img`, `/vendor` partition is populated and your device is treblized successfully .

### _Method 2_: Using `treble` patched Linage OS [![Recommended](https://img.shields.io/badge/Recommended-brown?style=flat-square)]()


* Download treble patched Lineage OS from here  [Lineage OS treble](https://drive.google.com/file/d/15cy1Pxjl3DIXzGhwEpiCiu3BqlE6ZkaW/view?usp=drive_link) and flash it from `twrp` recovery, either by copying the zip or using `adb sideload` method. 


* After flashing lineage OS, reboot your phone into system and check if everything works .

* And if it is, then your phone is successfully treblized and vendor is populated.

 <h5>Hence, you are good to go... </h5></p>

<a name="step2"></a>
<h2>Step 2: Building/Downloading the OS</h2>

To manually build UT 20.04 for OnePlus 3(T), follow these steps.

* Clone this repository onto your system.
* Run the following command from inside the repo folder .
```bash
export HOSTCC=gcc-9  # the build breaks with gcc-11
./build.sh -b bd  # bd is the name of the build directory
```
* Then, a new folder named `bd` is created inside which two files are generated.
1. `ubuntu.img` - A system image file containing the rootfs and OS
2. `boot.img` - A boot image file containing kernel and ramdisk

Or alternatively, you can download the automated build images from [here](https://gitlab.com/leolegendnarasimha/fp-4-op-3-ubport/-/jobs/artifacts/focal/browse/out?job=devel-flashable-focal)


<h2>Step 3: Installing the OS</h2>

**Note**: _Make sure that `fastboot` and `adb` are installed on your PC/Laptop._
* Connect your device to the PC/Laptop
* Reboot phone into fastboot mode and navigate to folder containing the `ubuntu.img` and `boot.img` files on your PC/Laptop.

* Run this command ( assuming that you are in the folder where `boot.img` and `ubuntu.img` are located .)
```bash
fastboot flash boot boot.img
```
This will flash boot image to the device .
* Now, copy the installer script from [here](https://gitlab.com/leolegendnarasimha/fp-4-op-3-ubport/-/blob/focal/INSTALLER/op3_ut_installer.sh?ref_type=heads) to the folder containing `ubuntu.img` .
* Run the script :
```bash
bash op3_ut_installer.sh
```
This will install the system image to the device . { stay patient until the installation proceeds }

* Now wait for the device to reboot .

### Note: First boot may take longer (```30 minutes or lesser```)






