#!/bin/bash
set -xe

[ -d build ] || git clone git clone https://gitlab.com/ubports/community-ports/halium-generic-adaptation-build-tools -b halium-11 build
./build/build.sh "$@"
